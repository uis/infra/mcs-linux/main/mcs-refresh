mcs-refresh (75) 2019; urgency=medium

  * Depend on python3-apt rather than python-apt.

 -- Ben Harris <bjh21@cam.ac.uk>  Tue, 17 Sep 2019 13:02:08 +0100

mcs-refresh (74) 2019; urgency=medium

  * Add GitLab hosting and CI metadata.
  * Switch to Python 3.

 -- Ben Harris <bjh21@cam.ac.uk>  Tue, 17 Sep 2019 12:42:08 +0100

mcs-refresh (73) 2019; urgency=medium

  * No-change rebuild for MCS Linux 2019.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 24 Jul 2019 15:18:09 +0100

mcs-refresh (72) 2018; urgency=medium

  * Update Plymouth logging messages - in particular, remove the typo
    that left the word 'software' without a 't'. 

 -- David McBride <dwm37@cam.ac.uk>  Tue, 28 Aug 2018 20:49:07 +0100

mcs-refresh (71) 2018; urgency=medium

  * Remove mcs-refresh.d/10-kernel, which previously ensured that the 
    currently-running kernel was not removed at mcs-refresh time.  This
    prevented us from being able to automatically downgrade kernels,
    and with the transactional-update model, it *should* now be safe to
    remove the currently-running kernel as part of an mcs-refresh run.

  * Add Before= and After= constraints in, respectively, mcs-{pre,}refresh,
    for the apt-daily and apt-daily-upgrade units.  These can coincidentlly
    be scheduled while the MCS units are running, and they can conflict
    with each other, causing a boot to fail.

    (This case is unlikely, but possible.  While a subsequent reboot is
     highly likely to succeed, this failure-mode has been seen in production.) 

 -- David McBride <dwm37@cam.ac.uk>  Tue, 31 Jul 2018 17:29:48 +0100

mcs-refresh (70) 2018; urgency=medium

  * Usability improvement:
    Set the debian_chroot environment variable when executing 'chroot'
    in the mcs-transaction-shell utility; this causes the bash prompt
    in that shell to be prefixed with the string (mcs-transaction).

    This helps the user remember whether they're currently in the middle
    of a transaction or not.

  * Increase debhelper compatibility level to 10.  Update Build-Depends
    accordingly.  (Drop dh-systemd, increase version dependency on
    debhelper to >= 10.)

 -- David McBride <dwm37@cam.ac.uk>  Wed, 20 Jun 2018 14:42:07 +0100

mcs-refresh (69) 2018; urgency=medium

  * No-change rebuild for MCS Linux 2018.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 14 Jun 2018 20:04:05 +0100

mcs-refresh (68) 2017; urgency=medium

  * Add additional calls to sync during transaction operations, to
    try to ensure that the filesystem is correctly persisted to disk
    at various stages.  This is to try to address a race-condition that
    apparently causes initramfs files to be found corrupt on subsequent
    boots.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 11 Jan 2018 18:26:50 +0000

mcs-refresh (67) 2017; urgency=medium

  * No-change rebuild for MCS Linux 2017. 

 -- David McBride <dwm37@cam.ac.uk>  Tue, 06 Jun 2017 16:31:08 +0100

mcs-refresh (66) 2016; urgency=medium

  * Re-add driver packagelist snippet support.  This involves restoring the
    90-driver-packages script, as well as running `apt update` as an additional
    step in mcs-refresh.

    This will slow boot times a bit, but will allow us to e.g. install nVidia
    binary drivers on those hosts where it's warranted.

 -- David McBride <dwm37@cam.ac.uk>  Mon, 20 Mar 2017 15:31:38 +0000

mcs-refresh (65) 2016; urgency=medium

  * mcs-refresh.d: Remove 90-driver-packages script, as this relies on
    having a recent APT repository index present to perform its calculations.
    We've seen a small number of cases where this hasn't been true, possibly
    because of a transient repository correctness problem earlier this week,
    and resulted in boot failures.

    (These ultimately resolved themselves automatically.)

    If we choose to restore this functionality, we should also add back
    the `apt update` (or functional equivilent) to the mcs-refresh script that
    was removed to make the boot sequence more efficient.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 06 Oct 2016 17:00:42 +0100

mcs-refresh (64) 2016; urgency=medium

  * mcs-transaction-shell: Pass through exit status of nested command
    to caller.  This ensures that e.g. a failure of mcs-refresh is
    passed through to systemd.

 -- David McBride <dwm37@cam.ac.uk>  Mon, 05 Sep 2016 19:29:25 +0100

mcs-refresh (63) 2016;

  * mcs-prerefresh and mcs-refresh now both run after nss-lookup.target
    since they depend on being able to do DNS lookups.

 -- Ben Harris <bjh21@cam.ac.uk>  Fri, 02 Sep 2016 13:30:51 +0100

mcs-refresh (62) 2016; urgency=medium

  * mcs-refresh-packages: Fast update check was only checking version presence
    or absence, not versions.  Fix.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 31 Aug 2016 16:02:35 +0100

mcs-refresh (61) 2016; urgency=medium

  * mcs-transaction-discard: Use correct variable name when garbage-
    collecting historical filesystem transactions.  Add armouring to
    variable expansion to ensure that it cannot expand to the empty-string.
    NB. This is why we test: this defect, exposed by the previous update,
        rendered test machines unbootable!

 -- David McBride <dwm37@cam.ac.uk>  Wed, 31 Aug 2016 15:14:22 +0100

mcs-refresh (60) 2016; urgency=medium

  * mcs-transaction-discard: Only emit a warning notice when discarding
    a transaction that hasn't first been locked; this should not
    stop us from garbage-collecting it.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 31 Aug 2016 14:30:47 +0100

mcs-refresh (59) 2016; urgency=medium

  * mcs-refresh-packages: Also fix up some incorrect log messages.

 -- David McBride <dwm37@cam.ac.uk>  Tue, 30 Aug 2016 13:36:19 +0100

mcs-refresh (58) 2016; urgency=medium

  * mcs-refresh-packages: Fix wayward logic in 'check' code.

 -- David McBride <dwm37@cam.ac.uk>  Tue, 30 Aug 2016 12:10:15 +0100

mcs-refresh (57) 2016; urgency=medium

  * mcs-refresh-packages: Before we do any work attempting to
    update the operating-system, such as downloading package
    lists, do a fast first check to see if any changes are pending.

  * mcs-refresh: Rather than treat mcs-refresh-packages as an
    idempotent command, first run it with 'check', outside of a
    filesystem update transaction, to see if updates are pending.
    Only run the full update command within a filesystem update
    transaction if they are.

  * mcs-transaction-commit: Do not attempt to determine if changes
    were made within a filesystem update transaction; rather
    apply the commit (and set the reboot-required flag)
    unconditionally.  This is now safe, because mcs-refresh will
    only start a new filesystem update transaction if updates
    are required.

  * The consequences of these changes is that system boot-times
    should be reduced, and the filesystem transaction application
    mechanism made more robust.

  * mcs-transaction-{gc,discard}: Rather than duplicate transaction
    destruction code in both of these scripts (imperfectly!), move
    the transaction destruction code to mcs-transaction-discard and
    have -gc invoke -discard.

 -- David McBride <dwm37@cam.ac.uk>  Tue, 30 Aug 2016 10:29:15 +0100

mcs-refresh (56) 2016; urgency=medium

  * mcs-transaction-prepare: Do not use mount -o rbind, but
    manually recurse through the mount tree ourselves.  Recursive
    bind mounts behave differently, and cannot be safely unwound
    without affecting the original mounts under some circumstances.
    (This was the reason /dev/shm was not available at login-time.)

  * mcs-transaction-gc: Tighten up the mount unwind code to be
    more careful.

 -- David McBride <dwm37@cam.ac.uk>  Fri, 26 Aug 2016 12:09:15 +0100

mcs-refresh (55) 2016; urgency=medium

  * mcs-transaction-commit: As well as checking to see if any
    modifications have been made to the root btrfs filesystem, also
    check to see if /boot/grub/grub.cfg has been modified.
    This will hopefully prevent cases of filesystem generations being
    discarded when the live GRUB configuration is pointing to them.

  * mcs-transaction-discard: Add two new safety-features to check that
    a filesystem generation that is to be discarded is not relied on
    by the current GRUB configuration, nor is it the current live root
    filesystem image.  If it is, refuse to proceed.

  * mcs-transaction-shell: Disable strict error-handling around
    command execution; we don't want to the script to stop dead if
    the nested command returns a non-zero exit status!

 -- David McBride <dwm37@cam.ac.uk>  Thu, 25 Aug 2016 17:46:05 +0100

mcs-refresh (54) 2016; urgency=medium

  * mcs-refresh: Also avoid removing -extra kernel package, which
    contains, among other things, the USB HID driver.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 10 Aug 2016 17:25:22 +0000

mcs-refresh (53) 2016; urgency=medium

  * Remove '-o pipefail' from /usr/sbin/mcs-transaction-commit,
    as the pipelined construction involving `btrfs sub` and `grep -q`
    is causing `btrfs sub` to be killed with SIGPIPE, which is being
    treated as a fatal error.  This causes transactions to be discarded
    incorrectly, which causes system bootstrapping to fail.

    There is no pleasant way to ignore just this class of problem
    while keeping the desired error-handling facility, so disable it
    for the time being.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 14 Jul 2016 14:11:38 +0000

mcs-refresh (52) 2016; urgency=medium

  * Actually apply updates to mcs-transaction-commit.
  * Add call to mcs-transaction-gc before generating a new transaction
    to apply package updates in mcs-refresh, as otherwise historical
    snapshots won't be reaped.

 -- David McBride <dwm37@cam.ac.uk>  Tue, 12 Jul 2016 16:55:29 +0000

mcs-refresh (51) 2016; urgency=medium

  * Make mcs-{pre}refresh depend on local-fs.target having being
    reached, as otherwise they may start running before all local
    filesystems have been made available.
  * Make the mcs-fail service to explicitly conflict with the
    display-manager service, to ensure that we don't inadvertently
    display a graphical login prompt (and conceal the mcs-fail message)!
  * Make mcs-fail target isolatable, and cause it to be executed by
    systemd in an isolating mode if mcs-{pre}refresh fail.  This is
    to try to ensure that the system does not attempt to continue to
    boot in the failure of the update process failing.
  * Move the ConditionFileExists= incantation in the mcs-postrefresh
    service definition from [Service] to [Unit], as otherwise it is
    invalid and ignored.
  * Override the WorkingDirectory for the friendly-recovery.service
    definition, to remove the dependency on /root being available
    at service start-time.  This service already needs to start
    before local-fs-pre.target is reached, and so also requiring
    /root (which is a separate filesystem mount) results in a cyclic
    dependency.  (Systemd will arbitrarily delete a service dependency
    when a cycle is detected, which can have bad consequences.)
  * Revert the previous decision to make mcs-transaction-commit to
    always apply changes, as this causes -every- incantation of
    mcs-refresh to result in a reboot. (!)
  * Remove commented code from /usr/sbin/mcs-refresh.

 -- David McBride <dwm37@cam.ac.uk>  Tue, 12 Jul 2016 15:57:33 +0000

mcs-refresh (50) 2016; urgency=medium

  * Update for MCS linux 2016.
  * Convert from using Upstart to using systemd.  This introduces a new
    system-updated.target, which is required by the multi-user.target,
    and depends on having mcs-{pre,,post}refresh.service executed.
    The system-updated.target explicitly blocks systemd-logind and SSH
    from starting, thus preventing end-user logins until updates complete.
    mcs-fail.target and service will be invoked in the event a refresh
    fails, which will cause a reboot (and non-obviously, allows a 
    sysadmin to login on the console as root if they know a valid root
    passphrase.)
  * Drop Breaks: on mcs-config-apt.
  * Update Build-Depends: and Depends: clauses in debian/control.
  * Add mcs-transaction-discard and mcs-transaction-shell commands.
  * Enhance mcs-transaction-* commands to use advisory locking for
    transactions, using /run/lock/mcs-transaction/${ID}.
  * Update mcs-transaction-commit to unconditionally make a commit final,
    rather than trying to be clever and trying to discard commits when
    we can't detect changes have been made.  This may now miss changes 
    given that changes could have been made to non-global subvolumes which
    are not the root filesystem.
  * Make mcs-transaction-* able to cope with arbitrary configurations
    of subvolume mount configurations, rather than just a root/var split.
  * Move update-alternatives incantation from postrm to prerm, as
    indicated by Lintian.

 -- David McBride <dwm37@cam.ac.uk>  Mon, 11 Jul 2016 17:05:13 +0000

mcs-refresh (49) 2015; urgency=medium

  * Install /usr/sbin/policy-rc.d.mcs-refresh, specified as a priority 100
    /usr/sbin/policy-rc.d alternative, which rejects any invocation of
    init.d or Upstart jobs while MCS_REFRESH_CHROOT=1 is defined in the
    environment.

  * Update mcs-refresh to define MCS_REFRESH_CHROOT=1 when executing
    mcs-refresh in a transaction.

  * These two changes combine to cause package post-install scripts to
    not attempt to start/stop/restart/etc. init.d or upstart jobs
    while a new transaction is being applied.

    This is important in cases where a failure to execute such an action
    causes the package application to abort, in particular as Upstart
    job changes will affect the top-level system-image, rather than
    processes in the transaction chroot being generated.

 -- David McBride <dwm37@cam.ac.uk>  Mon, 24 Aug 2015 18:33:16 +0100

mcs-refresh (48) 2015; urgency=medium

  * Add mounted-mnt-root Upstart job to cause the root filesystem
    to be properly re-mounted with the appropriate options set, as
    merely adding the 'compress' flag to /etc/fstab is not sufficient
    to cause it to be enacted at boot-time.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 19 Aug 2015 18:12:33 +0100

mcs-refresh (47) 2015; urgency=medium

  * Update clean-var-tmp upstart job to not rely on pushd/popd,
    which is not available as a command when dash is used.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 19 Aug 2015 16:38:17 +0100

mcs-refresh (46) 2015; urgency=medium

  * Add new upstart job, clear-var-tmp, intended to run between
    mcs-prerefresh and mcs-refresh, which causes /var/tmp to be
    emptied.  This is a safety feature to ensure that an end-user
    filling up /var/tmp does not cause subsequent OS updates to fail.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 19 Aug 2015 13:58:09 +0100

mcs-refresh (45) 2015; urgency=medium

  * Update mcs-refresh to call mcs-transaction-gc if mcs-refresh
    does not return success - for example, if it was invoked with
    'check' and no updates are pending.
    This prevents a large number of mounts continually accruing as
    mcs-idlecheck periodically checks whether it should trigger a
    reboot.
  * Update mcs-transaction-gc to use /proc/mounts, rather than the
    output of the `mount` command, as a source of mount-points that
    might need trimming.  Do not let the failure to unmount one
    path prevent attempts to remove the others or subsequently 
    tidy-up subvolumes in /mnt/root.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 25 Jun 2015 17:10:36 +0100

mcs-refresh (44) 2015; urgency=medium

  * Features:
    - Log STDERR from mcs-refresh to syslog.
  * Bugfixes:
    - Exlicitly set PATH at top of mcs-refresh, so that we execute
      correctly if run without a reasonable path.  (This can occur
      when invoked via mcs-idlecheck via CRON.)
    - Do not display a boot-error message if we're rebooting
      during start-up because an update was applied.
    - When displaying a boot error, do not give the user information
      to transcribe; useful data should now be logged via syslog.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 11 Jun 2015 13:08:27 +0100

mcs-refresh (43) 2015; urgency=medium

  * Update mcs-refresh.d/90-driver-packages to not fail if
    no package-list snippet can be securely fetched - either because
    it doesn't exist or because the signature is missing or invalid.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 10 Jun 2015 13:53:46 +0100

mcs-refresh (42) 2015; urgency=medium

  * Add first pass to mcs-refresh.d/90-driver-packages.
    Some refinements may be needed to cope gracefully when there
    are no extra packages that we wish to install for a particular
    class of hardware.

 -- David McBride <dwm37@cam.ac.uk>  Tue, 09 Jun 2015 17:30:39 +0100

mcs-refresh (41) 2015; urgency=medium

  * mcs-refresh-packages:
    - Update installer-time logging to write to filehandle 3,
      to match new expectations of the mcs-bootstrap installer.
  * mcs-transaction-gc:
    - Bugfix: put "|| true" guard around final rmdir step, to avoid
      returning failure to the caller inappropriately.
  * mcs-transaction-commit:
    - Bugfix: Restructure implementation to avoid invoking "grep" 
      outside of a conditional when it might correctly fail
      to match, as it causes grep to return a non-zero exit status
      and halts the script with an error.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 14 May 2015 17:07:34 +0100

mcs-refresh (40) 2015; urgency=medium

  * Major upgrade to mcs-refresh to add transactional updates
    on btrfs subvolumes.  Adds new commands:

    - mcs-transaction-prepare:
      Prepare a new subvolume for applying updates to.

    - mcs-transaction-commit:
      Make a new, updated subvolume the new default.

    - mcs-transaction-gc:
      Clean up and remove any dynamic subvolumes which are not
      the default.

    An updated top-level mcs-refresh script now makes use of these
    commands.

  * debian/control has been updated to add a hard dependency on
    btrfs-tools and eatmydata.

 -- David McBride <dwm37@cam.ac.uk>  Wed, 13 May 2015 16:30:18 +0100

mcs-refresh (39) 2015; urgency=medium

  * mcs-refresh:
    - Allow packagelists to contain lines containing only whitespace.
    - Allow comments to trail packagelist entries.
    - Use apt.cache.actiongroup() to optimise processing.
    - Log in more detail to Plymouth while updates are in-progress.
    - If the package-set is not installable, run the resolver and
      report any differences to what was requested to suggest to the
      admin what fixes are required.
      (Unfortunately, the resolver seems to be quite greedy, and will
       also suggest unnecessary package upgrades.)
    - If the package-set is not installable, report to syslog the set
      of errors and hints generated; don't just report exceptions
      to the console.
  * apt-pkglist:
    - Update usage string to describe all command-line arguments.
  * Update Uploaders and Maintainers fields in debian/control.
  * Fix Lintian errors/warnings:
    - Add missing Build-Depends: on python-support

 -- David McBride <dwm37@cam.ac.uk>  Tue, 12 May 2015 12:10:41 +0100

mcs-refresh (38) 2015; urgency=medium

  * No-change rebuild for 2015.

 -- David McBride <dwm37@cam.ac.uk>  Fri, 08 May 2015 14:42:49 +0100

mcs-refresh (37) 2014; urgency=medium

  * It is also necessary (though perhaps not sufficient?) to flush 
    writes to STDOUT, lest messages from different sources 
    become commingled. 

 -- David McBride <dwm37@cam.ac.uk>  Fri, 20 Jun 2014 14:24:50 +0100

mcs-refresh (36) 2014; urgency=medium

  * Distressingly, writing status messages to STDERR causes some package
    postinst scripts to fail.  Compensate for this by writing to STDOUT
    instead, and updating the various other utilities and configuration
    accordingly. 

 -- David McBride <dwm37@cam.ac.uk>  Fri, 20 Jun 2014 14:04:14 +0100

mcs-refresh (35) 2014; urgency=medium

  * New functionality: When mcs-refresh is invoked with 'installer', 
    emit status messages compatible with Dpkg::Progress to STDERR.
    This allows the bootstrapper to present useful progress-data.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 19 Jun 2014 19:44:51 +0100

mcs-refresh (34) 2014; urgency=medium

  * Don't complain about removing Essential packages that aren't
    currently installed.

 -- Ben Harris <bjh21@cam.ac.uk>  Wed, 21 May 2014 11:48:04 +0100

mcs-refresh (33) 2014; urgency=medium

  * No-change rebuild for MCS 2014.

 -- David McBride <dwm37@cam.ac.uk>  Tue, 25 Mar 2014 11:36:06 +0000

mcs-refresh (32) 2013; urgency=low

  * Improvements to MCS refresh messages printed to Plymouth splash screen.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 10 Oct 2013 16:49:20 +0100

mcs-refresh (31) 2013; urgency=low

  * Update plymouth log messages to be more verbose as to what's actually
    going on during an update.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 10 Oct 2013 13:20:37 +0100

mcs-refresh (30) 2013; urgency=low

  * Update the URLs used from updatesNN.linux.pwf -> linuxupdateNN.ds.
  * Make preserving the current-running kernel optional with a || true
    directive; the kernel currently running might be from bootstrap media,
    and thus may not be readily installable.

 -- David McBride <dwm37@cam.ac.uk>  Thu, 30 May 2013 15:40:00 +0000

mcs-refresh (29) 2013; urgency=low

  * Straight rebuild for 2013 release.  Some changes anticipated to be needed
    in future in order to handle multiarch 32bit/64bit installations.

 -- David McBride <dwm37@cam.ac.uk>  Tue, 23 Apr 2013 17:31:28 +0100

mcs-refresh (28) 2012;

  * Better error messages in mcs-refresh-packages: if an update would leave
    packages broken, make some effort to find out which ones would be broken
    and report that too.
  * Refuse to remove Essential packages in mcs-refresh-packages.  That should
    prevent various kinds of disaster.

 -- Ben Harris <bjh21@cam.ac.uk>  Thu, 16 Aug 2012 17:27:07 +0100

mcs-refresh (27) 2012;

  * Need "set -e" in mcs-refresh script as it depends on it for error handling.
  * Stop redirecting stderr to /dev/null even though that shows a lot of
    messages even on success (mcs-idlecheck will throw them away instead).
  * Make wget quieter with --no-verbose and gpg quietier with --quiet.  Both
    still produce output but less so but importantly they also still produce
    error messages.

 -- Anton Altaparmakov <aia21@cam.ac.uk>  Thu, 16 Aug 2012 10:58:25 +0100

mcs-refresh (26) 2012;

  * Whenever we validate a GPG signature, arrange to remove the
    temporary file used to store GnuPG's status output.  This
    should avoid an accumulation of such files in /tmp.

 -- Ben Harris <bjh21@cam.ac.uk>  Mon, 13 Aug 2012 13:06:19 +0100

mcs-refresh (25) 2012;

  * mcs-refresh now is self-contained shell script and the previous mcs-refresh
    is renamed to mcs-refresh-packages.  Both take a command line argument of
    "check" which when specified causes "refresh needed"/exit code 0 or
    "refresh not needed"/exit code 1 to be displayed/returned without any
    package modifications having been done.

 -- Anton Altaparmakov <aia21@cam.ac.uk>  Fri, 10 Aug 2012 15:14:59 +0100

mcs-refresh (24) 2012;

  * Detect dependency problems before committing changes, and
    error out in that case.  This should ensure that problems
    are detected before dpkg falls over in a heap.

 -- Ben Harris <bjh21@cam.ac.uk>  Thu, 02 Aug 2012 18:59:35 +0100

mcs-refresh (23) 2012;

  * Working mcs-prerefresh.
  * Switch to tty7 when refresh fails so that users can see messages.

 -- Ben Harris <bjh21@cam.ac.uk>  Wed, 25 Jul 2012 16:28:16 +0100

mcs-refresh (22) 2012; urgency=low

  * Use mcs-reboot for rebooting.

 -- Ben Harris <bjh21@cam.ac.uk>  Mon, 16 Jul 2012 17:43:56 +0100

mcs-refresh (21) 2012; urgency=low

  * Add --force-confnew to DPkg::Options so that refresh runs don't fail
    through conffile prompts.

 -- Ben Harris <bjh21@cam.ac.uk>  Thu, 12 Jul 2012 14:29:36 +0100

mcs-refresh (20) 2012; urgency=low

  * Depend on mcs-linux-release, which we now use to decide which release
    we're running.

 -- Ben Harris <bjh21@cam.ac.uk>  Tue, 10 Jul 2012 17:41:09 +0100

mcs-refresh (19) 2012; urgency=low

  * Add a mechanism for fetching an APT sources.list file, since
    installing it in mcs-config-apt is too late.

 -- Ben Harris <bjh21@cam.ac.uk>  Mon, 09 Jul 2012 19:24:23 +0100

mcs-refresh (18) 2012; urgency=low

  * Use the "console log" feature from upstart 1.4 to record
    mcs-refresh.log.

 -- Ben Harris <bjh21@cam.ac.uk>  Fri, 25 May 2012 20:18:55 +0100

mcs-refresh (17) 2012; urgency=low

  * Add dependencies on gpg and apt (the latter for /etc/apt/trusted.gpg).

 -- Ben Harris <bjh21@cam.ac.uk>  Wed, 09 May 2012 19:55:59 +0100

mcs-refresh (16) 2012; urgency=low

  * Add support for signed package lists.

 -- Ben Harris <bjh21@cam.ac.uk>  Wed, 09 May 2012 20:33:26 +0100

mcs-refresh (15) 2012; urgency=high

  * Wait for "hostname --fqdn" to return something other than "localhost"
    before trying to download a pkglist.  It seems that Ubuntu gives us
    a net-device-up event before DNS is working.

 -- Ben Harris <bjh21@cam.ac.uk>  Fri, 04 May 2012 15:38:23 +0100

mcs-refresh (14) 2012; urgency=high

  * Switch to new path for pkglists directory.
  * Change section to bare "admin" for new repo layout.

 -- Ben Harris <bjh21@cam.ac.uk>  Thu, 03 May 2012 17:39:15 +0100

mcs-refresh (13) oneiric-uxsup; urgency=low

  * Slightly finer-grained progress reporting through Plymouth.
  * Don't bother describing the updates to stdout.

 -- Ben Harris <bjh21@cam.ac.uk>  Fri, 30 Mar 2012 16:37:00 +0100

mcs-refresh (12) oneiric-uxsup; urgency=low

  * Don't log version numbers in deletion: sometimes the installed
    version seems to be None, which causes an exception.
  * Generate package lists in fragments from mcs-refresh.d, like the
    old system had.
  * Add a fragment to ensure that we don't remove the running kernel.

 -- Ben Harris <bjh21@cam.ac.uk>  Thu, 29 Mar 2012 16:07:30 +0100

mcs-refresh (11) oneiric-uxsup; urgency=low

  * Use kexec to reboot so that it's quicker and we don't need to
    depend on the active partition's being correct.  Pity about
    the display on chickatrice.
  * apt-pkglist:
     - Add support for "install" and "remove" operations.
     - Align output in columns.

 -- Ben Harris <bjh21@cam.ac.uk>  Tue, 27 Mar 2012 20:17:27 +0100

mcs-refresh (10) oneiric-uxsup; urgency=low

  * Reboot after running refresh at boot if update-notifier thinks
    that's a good idea.

 -- Ben Harris <bjh21@cam.ac.uk>  Mon, 26 Mar 2012 18:13:57 +0100

mcs-refresh (9) oneiric-uxsup; urgency=low

  * apt-pkglist: new script to emit a pkglist fragment for upgrade or
    dist-upgrade.

 -- Ben Harris <bjh21@cam.ac.uk>  Fri, 23 Mar 2012 12:53:23 +0000

mcs-refresh (8) oneiric-uxsup; urgency=low
   
  * Report all errors found while planning changes instead of stopping
    at the first one.

 -- Ben Harris <bjh21@cam.ac.uk>  Thu, 22 Mar 2012 13:21:05 +0000

mcs-refresh (7) oneiric-uxsup; urgency=low

  * Ask debconf to use the noninteractive frontend.

 -- Ben Harris <bjh21@cam.ac.uk>  Tue, 20 Mar 2012 11:58:53 +0000

mcs-refresh (6) oneiric-uxsup; urgency=low

  * Reinstate syslogging of changes, which had vanished somehow.

 -- Ben Harris <bjh21@cam.ac.uk>  Wed, 07 Mar 2012 12:24:54 +0000

mcs-refresh (5) oneiric-uxsup; urgency=low

  * Make refresh.log work while we wait for Ubuntu 12.04.
  * Log downgrades, and don't crash if we find a change we don't
    understand.

 -- Ben Harris <bjh21@cam.ac.uk>  Tue, 06 Mar 2012 17:35:40 +0000

mcs-refresh (4) oneiric-uxsup; urgency=low

  * "start mcs-refresh TESTMODE=yes" is the new
    "/etc/init.d/pwf-refresh test".

 -- Ben Harris <bjh21@cam.ac.uk>  Thu, 01 Mar 2012 14:34:12 +0000

mcs-refresh (3) oneiric-uxsup; urgency=low

  * syslog changes like pwf-refresh did.

 -- Ben Harris <bjh21@cam.ac.uk>  Wed, 29 Feb 2012 19:37:26 +0000

mcs-refresh (2) oneiric-uxsup; urgency=low

  * Don't run init scripts on install: that's a really bad idea.

 -- Ben Harris <bjh21@cam.ac.uk>  Wed, 29 Feb 2012 19:12:28 +0000

mcs-refresh (1) oneiric-uxsup; urgency=low

  * First APTish version.

 -- Ben Harris <bjh21@cam.ac.uk>  Wed, 29 Feb 2012 18:53:10 +0000
