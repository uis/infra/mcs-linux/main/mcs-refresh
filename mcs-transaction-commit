#!/bin/bash
set -e

TREE_NAME=$1

# Throw an error if no tree ID is supplied
if [ -z "${TREE_NAME}" ]; then
    echo "Usage: mcs-transaction-commit tree_id" >&2
    exit 1
fi

# Throw an error if tree ID is not valid.
if ! [ -d "/mnt/root/${TREE_NAME}" ]; then
    echo "Supplied tree name '${TREE_NAME}' not a directory in /mnt/root." >&2
    exit 2
fi

# Update the current/ symlink.
ln -sfT ${TREE_NAME} /mnt/root/current

# We can't be confident that btrfs subvolume operations are committed
# to disk, unless we also invoke sync.
sync

# Update grub.cfg, otherwise future boots might look in the wrong
# subvolume for boot-time critical data and we'll be rendered unbootable.
chroot /mnt/root/current/@ update-grub >&2 

# Signal that a reboot is required.
touch /run/reboot-required

# Remove advisory lock on this filesystem generation.
rm -f /run/lock/mcs-transaction/${TREE_NAME}

# Run sync again, to make sure everything has been persisted to
# stable storage.
sync

exit 0
